const hbs = require('hbs');

//registramos un helper en handlebars que retorna el año actual
hbs.registerHelper('getDate', () => new Date().getFullYear());
/**
 * @param text
 * registramos un helper en handlebars que recibe un texto
 * separa las letras del texto, toma la primera y la convierte en mayuscula
 * toma elresto y las convierte en minúscula
 */
hbs.registerHelper('capitalizar', (text) => {
    let letters = text.split(' ');

    letters.map((letter, key) =>{
     letters[key] = letter.charAt(0).toUpperCase() + letter.slice(1).toLowerCase();
    })
    return letters.join(' ');
});