const http = require('http');
const puerto = 8080;

//middleware, escuchamos todas las rutas...
http.createServer((req, res) =>{
    //devolvemos estado 200 y el tipo de respuesta en formato json
    res.writeHead(200, {'Content-Type': 'application/json'});

    let salida = {
        nombre: 'Juan',
        edad: 20,
        url: req.url
    }
    //pasamos el objeto a json
    res.write(JSON.stringify(salida));
    //terminamos la respuesta
    res.end();
})
.listen(puerto); //definimos el puerto en donde vamos a escuchar (local)

console.log(`Escuchando en el puerto ${puerto}`)