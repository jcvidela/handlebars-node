const express = require("express");
const app = express();
const hbs = require('hbs');
require('./hbs/helpers/helpers');

//middleware
app.use(express.static(`${__dirname}/public`));

//express hbs engine
app.set("view engine", "hbs");
hbs.registerPartials(`${__dirname}/views/partials`);

//en la ruta de inicio renderizamos el home y le enviamos la variable {{> nombre}} que espera el template
app.get("/", (req, res) => {
    res.render('home', { nombre: 'Juanca' });
});

app.get("/about", (req, res) => {
    res.render('about');
});

//si no recibimos un puerto en el despliegue, utilizamos el 3000, si no el recibido
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}`));
